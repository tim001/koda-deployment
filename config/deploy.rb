set :stages, ["testing", "staging", "prod_api2", "prod_api1", "prod_api2_slavedb", "prod_api1_slavedb"]
set :default_stage, "testing"
# config valid only for current version of Capistrano
# comment out the lock, current Capistrano version is higher than 3.5.0
#lock '3.5.0'

set :application, 'koda_api'

set :repo_url, 'git@bitbucket.org:solvup/koda-api.git'

set :tmp_dir, '/tmp/temp_cap'
set :git_https_username, 'solvup-dev'

#set :deployment_root_directory, '/var/www/koda-deploy'
#set :deployment_root_directory, '#{fetch(:deploy_to)}' #Unfortunately this does not work since deploy_to is defined in deploy/testing.rb...

#set :deployment_current_directory, "#{fetch(:deployment_root_directory)}/current"
# currently only keep var/logs in shared folder. Should we keep the whole "var" folder? In Symfony3 "var" folder contains 3 sub-dirs: logs, cache, sessions.
#set :deployment_shared_directory, "#{fetch(:deployment_root_directory)}/shared"
#the following more updated to related one
set :deployment_shared_directory, "shared"
#set :deployment_cache_directory, "#{fetch(:deployment_root_directory)}/shared/tmp/cache"

set :permission_method, :chmod
set :file_permissions_users, ["ubuntu"]
set :file_permissions_paths, ["var/cache"]
set :use_composer, false
set :composer_install_flags, '--no-dev'
set :app_config_path, 'app/config'

# Slack
set :slack_webhook, "https://hooks.slack.com/services/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"
set :slack_channel, '#solvup-dt-deployment'

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
# set :deploy_to, '/var/www/my_app_name'

# Default value for :scm is :git
set :git

# Default value for :format is :airbrussh.
# set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
# set :format_options, command_output: true, log_file: 'log/capistrano.log', color: :auto, truncate: :auto

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# set :linked_files, fetch(:linked_files, []).push('config/database.yml', 'config/secrets.yml')

# Default value for linked_dirs is []
# set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'public/system')

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
set :keep_releases, 5


namespace :deploy do

  desc "setting permission"
  task :updated do
    on roles(:all) do
   #   execute "chown -Rh ubuntu:www-data #{fetch(:deployment_current_directory)}"
    end
  end

  desc "copy config files before run composer, run composer require parameters.yml"
  task :copy_config_prepare do
      on roles(:all) do
        execute "cp -f #{release_path}/#{fetch(:app_config_path)}/parameters_#{fetch(:config_environment_upper)}.yml #{release_path}/#{fetch(:app_config_path)}/parameters.yml"
        execute "cp -f #{release_path}/#{fetch(:app_config_path)}/parameters_#{fetch(:config_environment_upper)}.yml.dist #{release_path}/#{fetch(:app_config_path)}/parameters.yml.dist"
      end
  end

 desc "running composer"
  task :composer_install do
    on roles(:all) do
      execute "cd '#{release_path}'; php composer.phar install --no-dev"
    end
  end

desc "copy config files task run composer, because these were auto-updated during composer execution but we want to use our confirmed version"
task :copy_config do
    on roles(:all) do
      execute "cp -f #{release_path}/#{fetch(:app_config_path)}/parameters_#{fetch(:config_environment_upper)}.yml #{release_path}/#{fetch(:app_config_path)}/parameters.yml"
      execute "cp -f #{release_path}/#{fetch(:app_config_path)}/parameters_#{fetch(:config_environment_upper)}.yml.dist #{release_path}/#{fetch(:app_config_path)}/parameters.yml.dist"
    end
end

desc "create symbolic link from log folder to share folder"
task :symbolic_var do
  on roles(:all) do
   execute "cd '#{release_path}'; rm -rf var"
   execute "cd '#{release_path}'; ln -s #{fetch(:deploy_to)}#{fetch(:deployment_shared_directory)}/var var"
  end
end

desc "clearing cache"
  task :clearing_cache do
    on roles(:all) do
      #symfony_console('cache:clear', '--env=prod --no-debug')
      execute "cd #{release_path}; sudo php bin/console cache:clear --env=#{fetch(:config_clear_cache)} --no-debug"

    end
  end

desc "set cache folder permission"
  task :updated do
    on roles(:all) do
     # execute "cd #{release_path}; chmod -R 777 var/cache"
     #  execute "cd #{release_path}; chmod 777 var/logs"
     # execute "cd #{release_path}; chown -R ubuntu:www-data var/sessions"
    end
  end

  desc "restart rqabbitmq queue command"
    task :restart_consumer do
      on roles(:all) do
        oldprocessid = capture("ps -ef |  grep \"solvup:consume\" | grep -v \"grep\" | awk '{print $2}'")
        if oldprocessid.length == 0 
          puts "\033[31m Warning: there is no existing process running \033[0m"
        else
          execute "kill -9 `ps -ef |  grep \"solvup:consume\" | grep -v \"grep\" | awk '{print $2}'`"
        end

        execute "cd #{release_path}; nohup php bin/console solvup:consume-notifications -vvv >> #{release_path}/var/logs/koda-console-output.log 2>&1 &"
        cmdprocessid = capture("ps -ef |  grep \"solvup:consume\" | grep -v \"grep\" | awk '{print $2}'")
        if cmdprocessid.length == 0
          puts "\033[31m Warning: the message queue consuming process hasn't started \033[0m"
        else 
          puts "\033[32m The message queue consuming process is running at " + cmdprocessid + " \033[0m"
        end
      end
    end

    task :check_restart_consumer do
      on roles(:all) do
        puts Time.now
        oldprocessid = capture("ps -ef |  grep \"solvup:consume\" | grep -v \"grep\" | awk '{print $2}'")
        if oldprocessid.length == 0
          puts "\033[31m Warning: there is no existing process running \033[0m"
          execute "cd #{release_path}; nohup php bin/console solvup:consume-notifications -vvv >> #{release_path}/var/logs/koda-console-output.log 2>&1 &"
          cmdprocessid = capture("ps -ef |  grep \"solvup:consume\" | grep -v \"grep\" | awk '{print $2}'")
          if cmdprocessid.length == 0
            puts "\033[31m Warning: the message queue consuming process hasn't started \033[0m"
          else
            puts "\033[32m The message queue consuming process is running at " + cmdprocessid + " \033[0m"
          end
          
        else
          puts "Old service is still running, the service hasn't restarted"
        end

      end
    end

desc "reset var folder permission"
  task :reset_var_folder_permission do
    on roles(:all) do
      execute "sudo chown -R ubuntu:www-data #{fetch(:deploy_to)}#{fetch(:deployment_shared_directory)}/var; sudo chmod -R 775 #{fetch(:deploy_to)}#{fetch(:deployment_shared_directory)}/var"
    end
  end

  after :finishing, :clearing_cache 
  after :clearing_cache, :restart_consumer
  after :restart_consumer, :reset_var_folder_permission
  after :updating, :copy_config_prepare
  after :updated, :composer_install
  after :updated, :copy_config
  after :updated, :symbolic_var 

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end

end

