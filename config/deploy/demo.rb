server '119.9.47.185',
    user: 'ubuntu',
    roles: %w{web app db},
    primary: true

set :deploy_to, "/var/www/demo/koda-api/"
set :config_environment_upper, "DEMO"
set :config_clear_cache, "prod"
set :branch, "master"